# Git 101 exercise

## Goal
Familiarize with Git by using basic commands to make changes and commit those to a git repository.

## Instructions

Note: Unless otherwise stated, please use a command line terminal (CMD prompt, bash, etc) for running the commands mentioned in the below sections.

0. Prerequisite.  
For some of the steps below to work you need an account on gitlab.com, with access to this project repository. If you do not have that yet, please contact [IT migration AWS](http://confluence.knmi.nl/display/ITMG/Support) to send you an invite. When you receive the email invite you need to sign up.

1. Install `git`.   
Download git from [here](https://git-scm.com/download). If you already have `git` installed on your laptop you may skip this step.
You can check if you already have `git` installed by running:
   ```
   git version
   ```
1. Set up name and email.  
If you have never used git before, you need to do some setup first. Run the following commands so that git knows your name and email. If you have git already setup, you can skip this step.
   ```
   git config --global user.name "Your Name"
   git config --global user.email "your_email@whatever.com"
   ```
1. Clone this repository to your laptop.   
Click on the `Clone` button on the top right of this page and copy the URL within the `Clone with HTTPS` box.  
Now run the following command (substitute the clone URL you just copied) and supply your gitlab credentials when prompted:
   ```
   git clone <paste clone url>
   ```
   The `git-workshop` directory should be now present at the location where you ran the clone. (You may move this directory to any convenient location in your laptop.)
1. Make a change in your local repo.  
Within the `git-workshop` directory, create a new file with your lastname as the filename. Add a line in the file with your favorite quote, and save the file.
1. Add and commit to local repo.  
Run the following command to check the status of the local repo.
    ```
    git status
    ```
    Stage the change for commit by adding it.
    ```
    git add <filename>
    ```
    To actually commit the changes, use:
    ```
    git commit -m "<commit message>"
    ```
    Now the file is committed to the local repo, but not in the remote repository yet.
1. Push changes to remote repo.  
Note: For this step to work fine step 0 (prerequisite) above should have been done.  
To send your changes to your remote repository, run
   ```
   git push origin master
   ```
   Note: The `git push` could fail if your local copy is outdated than the remote repo (because others have pushed changes before you). In such a case, run `git pull` first and then the `push`.  
   Refresh this gitlab page. Your added file should now be visible in this page.
1. Make more commits.
   You can add additional changes in the file you created, and repeat steps 5 thru 6 to commit those changes.
1. Check what changed.  
You may check the repository history by running:
   ```
   git log --author="<Your name>"
   ```
   Skipping the `--author` option above would show you commits from everyone.  
You can also use Gitlab's UI to view change history. On the left sidebar in this page, navigate to `Repository` > `Commits`.

This concludes this exercise.
